﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace HeeP.Common.Contracts
{
    public interface IRulesBase<T>
    {
        ICollection<T> GetAll();
        ICollection<T> GetAllPaged(int pageSize, int pageNumber);
        Task<T> GetAsync(params object[] keyValues);
        Task<T> AddAsync(T newgeneric);
        Task UpdateAsync(T newValue);
        Task DeleteAsync(T value);
    }
}
