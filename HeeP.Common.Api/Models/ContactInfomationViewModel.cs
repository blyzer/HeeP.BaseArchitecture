﻿using HeeP.Common.Entities.Enums;

namespace HeeP.Common.Api.Models
{
    public class ContactInfomationViewModel
    {
        public ContactType ContactType { get; set; }
        public string ContactValue { get; set; }
    }
}