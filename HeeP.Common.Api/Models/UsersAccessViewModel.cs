﻿using HeeP.Common.Entities.Enums;
using System.Collections.Generic;

namespace HeeP.Common.Api.Models
{
    public class UsersAccessViewModel
    {
        public string UserName { get; set; }
        public ICollection<AccessLevels> Access { get; set; }
    }
}