﻿namespace HeeP.Common.Api.Configurations
{
    public class LoginRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}