﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "RCS1102:Make class static.", Justification = "<Pending>", Scope = "type", Target = "~T:HeeP.Common.Api.Models.LoggingEvents")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Language Usage Opportunities", "RECS0014:If all fields, properties and methods members are static, the class can be made static.", Justification = "<Pending>", Scope = "type", Target = "~T:HeeP.Common.Api.Models.LoggingEvents")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Redundancy", "RCS1175:Unused this parameter.", Justification = "<Pending>", Scope = "member", Target = "~M:HeeP.Common.Api.Configurations.AutoMapperExtensions.ConfigureMaps(Microsoft.AspNetCore.Builder.IApplicationBuilder)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1052:Static holder types should be Static or NotInheritable", Justification = "<Pending>", Scope = "type", Target = "~T:HeeP.Common.Api.Models.LoggingEvents")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Redundancies in Symbol Declarations", "RECS0154:Parameter is never used", Justification = "<Pending>", Scope = "member", Target = "~M:HeeP.Common.Api.Configurations.AutoMapperExtensions.ConfigureMaps(Microsoft.AspNetCore.Builder.IApplicationBuilder)")]

