﻿using System.Threading.Tasks;
using HeeP.Common.Core.Repositories;
using HeeP.Common.Entities.Models;
using HeeP.Common.Entities.Enums;

namespace HeeP.Common.Core.Rules
{
    public class PersonRules : RulesBase<Person>
    {
        readonly PersonRepository _personRepository;

        public PersonRules(PersonRepository personRepository) : base(personRepository)
        {
            _personRepository = personRepository;
        }

        public Task<Person> GetByIdentificationNumberAsync(IdentificationType type, string number)
            => _personRepository.GetByIdentificationNumberAsync(type, number);
    }
}

