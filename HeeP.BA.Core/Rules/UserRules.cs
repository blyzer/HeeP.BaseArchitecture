﻿using HeeP.Common.Core.Repositories;
using HeeP.Common.Entities.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HeeP.Common.Core.Rules
{
    public sealed class UserRules : RulesBase<User>
    {
        readonly UserRepository _userRepository;

        public UserRules(UserRepository UserRepository) : base(UserRepository)
        {
            _userRepository = UserRepository;
        }

        public ICollection<User> GetPaged() => _userRepository.GetPaged().ToList();

        public Task<User> GetAsync(string userName) => _userRepository.GetAsync(userName);

        public async Task<ICollection<string>> GetUserRoleNamesAsync(string roleName) => await _userRepository.GetUserRoleNamesAsync(roleName);

        public async Task CreateUserAsync(User user, string password)
        {
            await _userRepository.CreateUser(user, password);
            await _userRepository.SaveChangesAsync();
        }

        public async Task EditUserAsync(User updatedUser, string password)
        {
            await _userRepository.UpdateUserAsync(updatedUser, password);
            await _userRepository.SaveChangesAsync();
        }
    }
}

