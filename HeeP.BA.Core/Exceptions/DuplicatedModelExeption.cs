﻿using System;

namespace HeeP.Common.Core
{
    public class DuplicatedModelExeption : Exception
    {
        public DuplicatedModelExeption(string message) : base(message)
        {

        }

        public DuplicatedModelExeption()
        {
        }

        public DuplicatedModelExeption(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DuplicatedModelExeption(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {
        }
    }
}
