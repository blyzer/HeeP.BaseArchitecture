﻿using HeeP.Common.Data;
using HeeP.Common.Entities.Enums;
using HeeP.Common.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace HeeP.Common.Core.Repositories
{
    public class PersonRepository : RepositoryBase<Person>
    {
        private readonly DataContext _context;

        public PersonRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Person> GetByIdentificationNumberAsync(IdentificationType type, string number)
        {
            return await _context.Persons
                .Include(p => p.ContactInformations)
                .Include(p => p.Address)
                .FirstOrDefaultAsync(p => p.IdentificationType == type && p.IdentificationNumber == number);
        }
    }
}
