﻿namespace HeeP.Common.Entities.Enums
{
    public enum ContactType
    {
        Home = 1,
        Mobile,
        Work,
        Email
    }
}
