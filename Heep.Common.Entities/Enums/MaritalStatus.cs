﻿namespace HeeP.Common.Entities.Enums
{
    public enum MaritalStatus
    {
        Single = 1,
        Married,
        Divorce,
        FreeUnion,
        Widowed
    }
}
