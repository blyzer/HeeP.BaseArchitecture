﻿namespace HeeP.Common.Entities.Enums
{
    public enum AccessLevels
    {
        Administrator,
        Operator
    }
}
