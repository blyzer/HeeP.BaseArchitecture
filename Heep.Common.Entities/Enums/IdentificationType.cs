﻿namespace HeeP.Common.Entities.Enums
{
    public enum IdentificationType
    {
        NationalIdentificationNumber = 1,
        Passport
    }
}
