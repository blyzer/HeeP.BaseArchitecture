﻿namespace HeeP.Common.Entities.Enums
{
    public enum UserType
    {
        Operator = 1,
        Administrator
    }
}
